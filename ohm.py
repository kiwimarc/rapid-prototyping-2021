while (1):
    print("1. Calculate Voltage")
    print("2. Calculate Resistance")
    print("3. Calculate Current")
    print("4. Calculate Power")
    print("5. Exit Program")
    ask = input("> ")
    if (ask == "1"):
        print("-- Calculating Voltage --")
        i = float(input("Current: "))
        r = float(input("Resistence: "))
        v = i * r
        print("V=I*R")
        print("Voltage = ", format(v, ".2f"))

    elif(ask == "2"):
        print("-- Calculating Resistance --")
        v = float(input("Voltage: "))
        i = float(input("Current: "))
        r = v / i
        print("R=V/I")
        print("Resistance = ", format(r, ".2f"))

    elif(ask == "3"):
        print("-- Calculating Current --")
        v = float(input("Voltage: "))
        r = float(input("Resistence: "))
        i = v / r
        print("I=V/R")
        print("Current = ", format(i, ".2f"))
        
    elif(ask == "4"):
        print("1. Calculate power from V*I")
        print("2. Calculate power from I^2*R")
        print("3. Calculate power from V^2/R")
        choice = input("> ")
        if(choice == "1"):
            print("-- Calculating Power --")
            v = float(input("Voltage: "))
            i = float(input("Current: "))
            p = v * i
            print("P=V*I")
            print("Power = ", format(p, ".2f"))
            
        if(choice == "2"):
            print("-- Calculating Power --")
            r = float(input("Resistance: "))
            i = float(input("Current: "))
            p = i^2 * r
            print("P=I^2*R")
            print("Power = ", format(p, ".2f"))
        
        if(choice == "3"):
            print("-- Calculating Power --")
            r = float(input("Resistance: "))
            v = float(input("Voltage: "))
            p = v^2 /r
            print("P=V^2/R")
            print("Power = ", format(p, ".2f"))
       
        
    elif(ask == "5"):
        break
    else:
        print("Not an option, Try again")