def findResistance(a, m, t):
      
    # Hash-map to store the values of the color-digits
    color_digit = {'black': '0',
                   'brown': '1', 
                   'red': '2',
                   'orange': '3', 
                   'yellow': '4',
                   'green' : '5', 
                   'blue' : '6',
                   'violet' : '7', 
                   'grey' : '8',
                   'white': '9'}
      
    multiplier = {'black': '1',
                  'brown': '10', 
                  'red': '100', 
                  'orange': '1k', 
                  'yellow': '10k', 
                  'green' : '100k', 
                  'blue' : '1M', 
                  'violet' : '10M',
                  '' : ''}
      
    tolerance = {'brown': '+/- 1 %', 
                  'red' : '+/- 2 %', 
                 'green': "+/- 0.5 %", 
                  'blue': '+/- 0.25 %', 
                 'violet' : '+/- 0.1 %', 
                  'gold': '+/- 5 %', 
                 'silver' : '+/- 10 %',
                 '' : ''}
    
    xx = ""
    for i in range(len(a)):
        if not (a[i] in color_digit):
            print("Color ", format(a[i]))
            print("Is not a valid color")
        else:
            xx += color_digit.get(a[i])
            
    if (m in multiplier 
       and t in tolerance):
           mm = multiplier.get(m)
           tt = tolerance.get(t)
           print("Resistance = "+xx +\
                 " x "+mm+" ohms "+tt)
    else:
        print("Invalid Colors")
          
# Driver Code
if __name__ == "__main__":
    print("Accepted values for color digit and multiplier:")
    print(" * black")
    print(" * brown")
    print(" * red")
    print(" * orange")
    print(" * yellow")
    print(" * green")
    print(" * blue")
    print(" * violet")
    print(" * grey")
    print(" * white")
    print("Accepted values for tolerance")
    print(" * All the same as above")
    print(" * gold")
    print(" * silver")
    print()
    
    print("Add all the color digit bands.")
    print("When done, hit enter to continue.")
    a = []
    for i in range (4):
        a.append(input("> "))
        if not (a[i]):
            a.pop(i)
            break
    
    print()
    print("Add multiplier band")
    m = input("> ")
    
    print()
    print("Add tolerance band")
    t = input("> ")
      
    # Function Call
    findResistance(a, m, t)