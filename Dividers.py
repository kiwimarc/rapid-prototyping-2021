while (1):
    print("1. Voltage divider")
    print("2. Current divider")
    print("3. Exit Program")
    ask = input("> ")
    if (ask == "1"):
        print("Voltage divider equation: V2 = R2/(R1+R2)*V1")
        V1 = float(input("V1: "))
        R1 = float(input("R1: "))
        R2 = float(input("R2: "))
        print("-- Calculating V2 --")
        V2 = (V1*R2)/(R1+R2)
        print("V2 = ", format(V2, ".2f"))

    if (ask == "2"):
        print("Current divider equation: I1 = R2/(R1+R2)*I")
        I = float(input("I: "))
        R1 = float(input("R1: "))
        R2 = float(input("R2: "))
        print("-- Calculating I1 & I2 --")
        I1 = (I*R2)/(R1+R2)
        I2 = (I*R1)/(R1+R2)
        print("I1 = ", format(I1, ".2f"))
        print("I2 = ", format(I2, ".2f"))
        
    if (ask == "3"):
        break