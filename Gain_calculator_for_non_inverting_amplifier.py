from pyperclip import copy
E12 = [10,12,15,18,22,27,33,39,47,56,68,82]

print("This program calculates the resistors needed for a specified gain on a non inverting amplifier\n")
magnitude = float(input("magnitude of resistors to add?\ndefault = 10 to 82 if 10 is input it will include upto 820\n"))#append magnitude to append of E12
for i in range(len(E12)):
    E12.append(E12[i]*magnitude)

amount = len(E12)#amount of resistors

gain = []
Rval = []

for i in range(amount):
    R2 = E12[i]#def R2
    for k in range(amount):#will now iterate amount^2 times big O = n^2 watch out!!
        R1 = E12[k]#def R1
        gain.append(1+(R1/R2))
        Rval.append([R1,R2])

dick = {}#creates a dictionary with gains as keys to resistances in list. Note:only last iteration of value and gain will appear if more than 1 combination is possible. This means that for a gain of 2 the value will be R1=820 and R2=820. This is not a problem seeing as you can always divide with magnitude
for i in range(len(gain)):
    dick.update({gain[i]:Rval[i]})

desiredval = float(input("Desired gainvalue:  "))
margin = float(input("Margin input in float aka 0.05 for 5%:  "))

mapleinput = []

i = 0
while (i < amount):
    if gain[i] < desiredval*(1+margin) and gain[i] > desiredval*(1-margin):#If between margins
        print("___________" + str(i) + "____________")
        print("for gain of:  " + str(gain[i]))
        print("R1=" + str(Rval[i][0]) + "  R2=" + str(Rval[i][1]))
        print("__________________________\n\n")
        mapleinput.append("R[1],R[2]:=" + str(Rval[i][0]) + "," + str(Rval[i][1]) + ":")
    if i == amount-1:
        print("No more values found.... if none were found you should try again")
        print("try again with different margin? y/n")
        ask = input()
        if ask == "y":
            margin = float(input("Input new margin  "))
            i = -1
    i += 1
def maplecopy(x):#copies maple input
    copy(mapleinput[x])
print("For maplecopy input index")
maplecopy(int(input()))

input("press enter to exit program")
