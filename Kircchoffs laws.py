while (1):
    print("1. Calculate Kirchhoffs current law")
    print("2. Calculate Kirchhoffs voltage law")
    print("3. Exit Program")
    ask = input("> ")
    if (ask == "1"):
        print("Just as much current leves a branching point as enters it")
        print("The sum of all currents entering a node equals zero")
        print("The sum of all current leaving a node equeals zero")
        i=[]
        while(1):
            i.append(float(input("Current: ")))
            more = input("Add more? (y/n)> ")
            if (more == "y"):
                i.append(float(input("Current: ")))
                more = input("Add more? (y/n)> ")
            if (more == "n"):
                break
            else:
                print("Not an option, Try again")
        print("-- Calculating KCL --")
        current = 0
        for x in i:
            current = current+x
        print("KCL = ", format(current, ".2f"))

    if (ask == "2"):
        print("In a closed loop, the sum of voltage-increase and -decrease add up to zero")
        print("Nothing to calculate here")
        print("For more infomation, go to week 2 L02 page 11 to 12")
        input("Press anything to continue")
        
    if (ask == "3"):
        break
    else:
        print("Not an option, Try again")